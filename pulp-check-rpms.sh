#!/bin/bash
# ------------------------------------------------------------------
#Title          :pulp-check-rpms.sh
#Description    :This script will compare checksums for every .rpm, .srpm and .drpm, which according to pulp should already be downloaded. It calculates the checksum and compares it to what it should be.
#                If the checksum is incorrect, it will delete the file and remove the downloaded flag in the dabase, so pulp will redownlad it on the next sync. 
#Author         :Michael Schietzsch
#Company        :Atix AG
#Date           :2017-04-25
#Version        :0.5
#Usage          :bash pulp-check-rpms.sh
#Notes          :Run this script on the pulp server.
# ------------------------------------------------------------------

echo "DBQuery.shellBatchSize = 10000000" > .mongorc.js

#find all .rpm, .srpm and .drpm with patch checksum  and checksumtype
printf "Query MongoDB for rpms, which should already be downloaded. This may take a minute...\n"
mongo pulp_database --eval "load('.mongorc.js'); db.units_rpm.find({downloaded:true}, { _storage_path: 1, checksum: 1, checksumtype: 1  }).shellPrint()" |cut -d\" -f 8,12,16 > /tmp/rpm.list
tail -n +3 /tmp/rpm.list > /tmp/rpm.list.tmp && mv /tmp/rpm.list.tmp /tmp/rpm.list

printf "Query MongoDB for srpms, which should already be downloaded.\n"
mongo pulp_database --eval "load('.mongorc.js'); db.units_srpm.find({downloaded:true}, { _storage_path: 1, checksum: 1, checksumtype: 1  }).shellPrint()" |cut -d\" -f 8,12,16 > /tmp/srpm.list
tail -n +3 /tmp/srpm.list > /tmp/srpm.list.tmp && mv /tmp/srpm.list.tmp /tmp/srpm.list

printf "Query MongoDB for drpms, which should already be downloaded.\n"
mongo pulp_database --eval "load('.mongorc.js'); db.units_drpm.find({downloaded:true}, { _storage_path: 1, checksum: 1, checksumtype: 1  }).shellPrint()" |cut -d\" -f 8,12,16 > /tmp/drpm.list
tail -n +3 /tmp/drpm.list > /tmp/drpm.list.tmp && mv /tmp/drpm.list.tmp /tmp/drpm.list

counter=0

check_rpm () {
   while read line; do
    path=$(echo $line | awk -F\" '{print $1}')
    db_sum=$(echo $line | awk -F\" '{print $2}')
    checksumtype=$(echo $line | awk -F\" '{print $3}')
    case "$checksumtype" in
        ("sha256") calc_sum=$(sha256sum $path | awk '{print $1}') ;;
        ("sha1") calc_sum=$(sha1sum $path | awk '{print $1}') ;;
        ("md5") calc_sum=$(md5sum $path | awk '{print $1}') ;;
    esac
    counter=$((counter +1))
    printf "$counter of $2\r"
    if [ "$db_sum" == "$calc_sum" ]
        then 
            :
        else 
            printf "PROBLEM with $path \n DB SUM: $db_sum \n Calculated: $calc_sum \n ---" >> $1.errors.tmp
            rm  $path
            mongo pulp_database --quiet --eval 'db.units_rpm.update({"_storage_path":"'"$path"'"},{$set:{"downloaded": false}});'
        fi
    done < $1
} 

#checking RPM Unnits
rpmline=$(cat /tmp/rpm.list | wc -l)
printf "Checking RPMs. \e[36;40mThis could take quite some time...\e[m\n"
check_rpm "/tmp/rpm.list" $rpmline
counter=0

#checking SRPM Units
srpmline=$(cat /tmp/srpm.list | wc -l)
printf 'Checking SRPMs. They are fewer, but way bigger... \e[36;40mpatience is a virtue!\e[m\n'
check_rpm "/tmp/srpm.list" $srpmline
counter=0

#checking DRPM Units
drpmline=$(cat /tmp/drpm.list | wc -l)
printf "Checking DRPMs. \e[36;40mAlmost done\e[m\n"
check_rpm "/tmp/drpm.list" $drpmline
counter=0

#check if there was anything to fix and cleanup
if [ -s /tmp/rpm.list.errors.tmp ] || [ -s /tmp/srpm.list.errors.tmp ] || [ -s /tmp/drpm.list.errors.tmp ] 
    then
        printf "\nYou had \e[31;40merrors\e[m in one of the RPM units, \e[32;40mplease sync your repos\e[m to fix missing or broken packages now.\n"
        printf "See the logs for more information: \e[33;40mpulp-rpm-fix.errors pulp-srpm-fix.errors pulp-drpm-fix.errors\e[m\n"
        if [ -s /tmp/rpm.list.errors.tmp ]
            then 
            mv /tmp/rpm.list.errors.tmp /root/pulp-rpm-fix.errors
        fi
        if [ -s /tmp/srpm.list.errors.tmp ]
            then 
            mv /tmp/srpm.list.errors.tmp /root/pulp-srpm-fix.errors
        fi
        if [ -s /tmp/drpm.list.errors.tmp ]
            then 
            mv /tmp/drpm.list.errors.tmp /root/pulp-drpm-fix.errors
        fi
    else
        printf '\n\e[32;40mNo errors found :-)\e[m\n'
fi
rm .mongorc.js /tmp/rpm.list /tmp/srpm.list /tmp/drpm.list